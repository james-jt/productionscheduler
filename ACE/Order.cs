﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Order
    {
        public enum Priority { VeryHigh, High, Normal, Low, VeryLow }
        public struct Dimensions
        {
            public int Length;
            public int Width;
            public int Diameter;
        }
         
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderedBy { get; set; }
        public Product OrderProduct { get; set; }
        public Priority OrderPriority { get; set; }
        public int Quantity { get; set; }
        public DateTime DueDate { get; set; }
        public Dimensions OrderDimensions { get; set; }
        public bool IsScheduled { get; set; }
    }
}
