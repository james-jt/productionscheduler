﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Workers
    {
        public Worker Drawing_Artisan { get; set; }
        public Worker Pattern_Artisan { get; set; }
        public Worker Pattern_Apprentice { get; set; }
        public Worker Pattern_General { get; set; }
        public Worker Foundry_Artisan { get; set; }
        public Worker Foundry_Apprentice { get; set; }
        public Worker Foundry_General { get; set; }
        public Worker Fettling_Artisan { get; set; }
        public Worker Fettling_Apprentice { get; set; }
        public Worker Fettling_General { get; set; }
        public Worker MachineShop_Artisan { get; set; }
        public Worker MachineShop_Apprentice { get; set; }
        public Worker MachineShop_General { get; set; }
        public List<Worker> WorkersCollection { get; set; }

        public Workers()
        {
            Drawing_Artisan = new Worker("Drawing_Artisan", "Draws patterns.", 2);
            Pattern_Artisan = new Worker("Pattern_Artisan", "Makes patterns from drawings.", 2);
            Pattern_Apprentice = new Worker("Pattern_Apprentice", "Makes patterns from drawings.", 2);
            Pattern_General = new Worker("Pattern_General", "Makes patterns from drawings.", 2);
            Foundry_Artisan = new Worker("Foundry_Artisan", "Makes castings from patterns.", 2);
            Foundry_Apprentice = new Worker("Foundry_Apprentice", "Makes castings from patterns.", 2);
            Foundry_General = new Worker("Foundry_General", "Makes castings from patterns.", 2);
            Fettling_Artisan = new Worker("Fettling_Artisan", "Removes rough edges from castings.", 2);
            Fettling_Apprentice = new Worker("Fettling_Apprentice", "Removes rough edges from castings.", 2);
            Fettling_General = new Worker("Fettling_General", "Removes rough edges from castings.", 2);
            MachineShop_Artisan = new Worker("MachineShop_Artisan", "Produces the final geometry of the product.", 2);
            MachineShop_Apprentice = new Worker("MachineShop_Apprentice", "Produces the final geometry of the product.", 2);
            MachineShop_General = new Worker("MachineShop_General", "Produces the final geometry of the product.", 2);
            WorkersCollection = new List<Worker>();
            WorkersCollection.AddRange(new[] { Drawing_Artisan, Pattern_Artisan, Pattern_Apprentice, Pattern_General, Foundry_Artisan, Foundry_Apprentice, Foundry_General, Fettling_Artisan, Fettling_Apprentice, Fettling_General, MachineShop_Artisan, MachineShop_Apprentice, MachineShop_General });
        }
    }
}
