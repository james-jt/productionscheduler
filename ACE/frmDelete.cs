﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE
{
    public partial class frmDelete : Form
    {
        DataSet ds;
        DataTable dt;
        public frmDelete()
        {
            InitializeComponent();
            ds = new DataSet();
            ds = frmMain.dataAccess.ReadAllOrderRecords();
            if (ds == null) return;
            if (ds.Tables.Count < 1) return;
            if (ds.Tables[0].Rows.Count < 1) return;
            cmbOrderNumber.DataSource = ds;
            cmbOrderNumber.DisplayMember = ds.Tables[0].Rows[0]["ORDERNUMBER"].ToString();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cmbOrderNumber.Text)) return;
            string msg = string.Format("Are yousure you want to delete Order Number {0}?", cmbOrderNumber.Text);
            if(MessageBox.Show(msg, "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if(new DataAccess().DeleteOrderRecord(cmbOrderNumber.Text))
                {
                    MessageBox.Show("Order successfully deleted.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Order was not deleted.");
                }
            }
        }

        private void frmDelete_Load(object sender, EventArgs e)
        {

        }
    }
}
