﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Machine : Resource
    {
        public Machine(string fullName, string description, int totalInstances) 
            : base(fullName, description, totalInstances) { Type = ResourceType.Machine; }
    }
}
