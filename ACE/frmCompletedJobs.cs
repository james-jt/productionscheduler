﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE 
{
    public partial class frmCompletedJobs : Form
    {
        DataSet ds;
        DataTable dt;
        public frmCompletedJobs()
        {
            InitializeComponent();
            ds = new DataSet();
            dgdCompletedJobs.DataSource = ds;
            dt = new DataTable();
            ds.Tables.Add(dt);
            dgdCompletedJobs.DataMember = ds.Tables[0].ToString();
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            ds = new DataAccess().ReadAllCompletedJobsRecords();
            dgdCompletedJobs.DataSource = ds;
            if(ds != null && ds.Tables.Count > 0)
                dgdCompletedJobs.DataMember = ds.Tables[0].ToString();
        }

    }
}
