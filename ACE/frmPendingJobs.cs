﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE
{
    public partial class frmPendingJobs : Form
    {
        DataSet ds;
        DataTable dt;
        public frmPendingJobs()
        {
            InitializeComponent();
            ds = new DataSet();
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            ds = new DataAccess().ReadAllPendingJobsRecords();
            dgdPendingJobs.DataSource = ds;
            if(ds != null && ds.Tables.Count > 0)
                dgdPendingJobs.DataMember = ds.Tables[0].ToString();
        }

    }
}
