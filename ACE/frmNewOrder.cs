﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE
{
    public partial class frmNewOrder : Form
    {
        DataSet dsGrid;
        DataSet dsProducts;
        Products products;

        public frmNewOrder()
        {
            InitializeComponent();
            dsGrid = new DataSet();
            FillDataGrid();

            dsProducts = new DataSet();
            products = new Products();
            FillOrderNumberComboBox();
            ClearForm();
        }

        #region Common Methods

        private void ClearForm()
        {
            cmbProduct.Text = cmbProduct.Items[0].ToString();
            cmbPriority.Text = cmbPriority.Items[2].ToString();
            cmbQuantity.Text = cmbQuantity.Items[0].ToString();
            cmbLength.Text = cmbLength.Items[0].ToString();
            cmbWidth.Text = cmbWidth.Items[0].ToString();
            cmbDiameter.Text = cmbDiameter.Items[0].ToString();
            txtOrderedBy.Text = "";
            dtpDueDate.MaxDate = System.DateTime.Today.AddMonths(3);
            dtpDueDate.MinDate = System.DateTime.Today;
            dtpDueDate.Value = System.DateTime.Today.AddDays(1);
            dgdPendingOrders.Update();

            
            cmbProduct.Focus();
        }
        private void FillOrderNumberComboBox()
        {
            dsProducts.Tables.Add("Products");
            dsProducts.Tables["Products"].Columns.Add("Name");
            int counter = 0;
            foreach (Product product in products.ProductsCollection)
            {
                dsProducts.Tables["Products"].Rows.Add();
                dsProducts.Tables["Products"].Rows[counter]["Name"] = product.Name;
                counter++;
            }
            cmbProduct.DataSource = dsProducts.Tables["Products"];
            cmbProduct.DisplayMember = dsProducts.Tables["Products"].Columns["Name"].ToString(); ;
        }

        private void FillDataGrid()
        {
            dsGrid = frmMain.dataAccess.ReadAllPendingOrdersRecords();
            if (dsGrid == null) return;
            if (dsGrid.Tables.Count < 1) return;
            dgdPendingOrders.DataSource = dsGrid;
            dgdPendingOrders.DataMember = dsGrid.Tables[0].ToString();
        }


        private void UpdateDataGrid()
        { 
            //Update dataset ds
            dgdPendingOrders.Update();
        }

        private void Save()
        {
            Order newOrder = new Order();
            newOrder.OrderNumber = Convert.ToInt32(txtOrderNo.Text);
            newOrder.OrderDate = DateTime.Today;
            newOrder.OrderedBy = txtOrderedBy.Text;
            newOrder.OrderProduct = new Products().ProductsCollection.Where(p => p.Name == cmbProduct.Text).Select(p => p).ToList().First();
            newOrder.OrderPriority = (Order.Priority)(cmbPriority.SelectedIndex + 1);
            newOrder.Quantity = Convert.ToInt32(cmbQuantity.Text);
            newOrder.DueDate = dtpDueDate.Value;
            newOrder.OrderDimensions = new Order.Dimensions { Length = Convert.ToInt32(cmbLength.Text), Width =  Convert.ToInt32(cmbWidth.Text), Diameter = Convert.ToInt32(cmbDiameter.Text)};
            if(new DataAccess().CreateOrderRecord(newOrder))
            {
                MessageBox.Show("Order successfully created.", "Operation Sucessful");
            }
            else
            {
                MessageBox.Show("Order was not created.", "Error");
            }
        }
        #endregion

        private void frmNewOrder_Load(object sender, EventArgs e)
        {
            do
            {
                txtOrderNo.Text = new Random().Next().ToString();
                DataSet ds = new DataSet();
                ds = frmMain.dataAccess.ReadOrderRecord(txtOrderNo.Text);
                if (ds == null) break;
                if (ds.Tables.Count < 1) break;
            } while (true);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            Save();
            this.Close();
        }

        private void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            Save();
            ClearForm();
        }

    }
}
