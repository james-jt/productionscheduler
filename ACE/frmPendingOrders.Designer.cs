﻿namespace ACE
{
    partial class frmPendingOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLeftPadding = new System.Windows.Forms.Panel();
            this.pnlRightPadding = new System.Windows.Forms.Panel();
            this.pnlBottomPadding = new System.Windows.Forms.Panel();
            this.gbxDataGrid = new System.Windows.Forms.GroupBox();
            this.dgdPendingOrders = new System.Windows.Forms.DataGridView();
            this.gbxDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgdPendingOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLeftPadding
            // 
            this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftPadding.Name = "pnlLeftPadding";
            this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
            this.pnlLeftPadding.TabIndex = 7;
            // 
            // pnlRightPadding
            // 
            this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
            this.pnlRightPadding.Name = "pnlRightPadding";
            this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
            this.pnlRightPadding.TabIndex = 8;
            // 
            // pnlBottomPadding
            // 
            this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
            this.pnlBottomPadding.Name = "pnlBottomPadding";
            this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
            this.pnlBottomPadding.TabIndex = 10;
            // 
            // gbxDataGrid
            // 
            this.gbxDataGrid.Controls.Add(this.dgdPendingOrders);
            this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxDataGrid.Location = new System.Drawing.Point(12, 0);
            this.gbxDataGrid.Name = "gbxDataGrid";
            this.gbxDataGrid.Size = new System.Drawing.Size(886, 429);
            this.gbxDataGrid.TabIndex = 16;
            this.gbxDataGrid.TabStop = false;
            this.gbxDataGrid.Text = "Pending Orders";
            // 
            // dgdPendingOrders
            // 
            this.dgdPendingOrders.AllowUserToAddRows = false;
            this.dgdPendingOrders.AllowUserToDeleteRows = false;
            this.dgdPendingOrders.AllowUserToOrderColumns = true;
            this.dgdPendingOrders.BackgroundColor = System.Drawing.Color.White;
            this.dgdPendingOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgdPendingOrders.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgdPendingOrders.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgdPendingOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgdPendingOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgdPendingOrders.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dgdPendingOrders.Location = new System.Drawing.Point(3, 19);
            this.dgdPendingOrders.Name = "dgdPendingOrders";
            this.dgdPendingOrders.ReadOnly = true;
            this.dgdPendingOrders.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgdPendingOrders.RowHeadersVisible = false;
            this.dgdPendingOrders.Size = new System.Drawing.Size(880, 407);
            this.dgdPendingOrders.TabIndex = 1;
            // 
            // frmPendingOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(910, 441);
            this.Controls.Add(this.gbxDataGrid);
            this.Controls.Add(this.pnlBottomPadding);
            this.Controls.Add(this.pnlRightPadding);
            this.Controls.Add(this.pnlLeftPadding);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmPendingOrders";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Pending Orders";
            this.gbxDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgdPendingOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeftPadding;
        private System.Windows.Forms.Panel pnlRightPadding;
        private System.Windows.Forms.Panel pnlBottomPadding;
        private System.Windows.Forms.GroupBox gbxDataGrid;
        public System.Windows.Forms.DataGridView dgdPendingOrders;
    }
}

