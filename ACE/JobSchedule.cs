﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class JobSchedule
    {
        public int JobNumber { get; set; }
        public string Station { get; set; }
        public string ResourceName { get; set; }
        public string ResourceType { get; set; }
        public DateTime QueueEnterTime { get; set; }
        public DateTime ProcessingStartTime { get; set; }
        public DateTime ProcessingFinishTime { get; set; }

    }
}
