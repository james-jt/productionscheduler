﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Worker : Resource
    {
        public Worker(string fullName, string description, int totalInstances)
            : base(fullName, description, totalInstances) { Type = ResourceType.Worker; }
    }
}
