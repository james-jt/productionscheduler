﻿namespace ACE
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLeftPadding = new System.Windows.Forms.Panel();
            this.pnlRightPadding = new System.Windows.Forms.Panel();
            this.gbxButtons = new System.Windows.Forms.GroupBox();
            this.btnPrintandNew = new System.Windows.Forms.Button();
            this.btnPrintandClose = new System.Windows.Forms.Button();
            this.btnSaveAndNew = new System.Windows.Forms.Button();
            this.btnSaveAndClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlBottomPadding = new System.Windows.Forms.Panel();
            this.gbxDataGrid = new System.Windows.Forms.GroupBox();
            this.dgdPendingOrders = new System.Windows.Forms.DataGridView();
            this.gbxButtons.SuspendLayout();
            this.gbxDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgdPendingOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLeftPadding
            // 
            this.pnlLeftPadding.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftPadding.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftPadding.Name = "pnlLeftPadding";
            this.pnlLeftPadding.Size = new System.Drawing.Size(12, 441);
            this.pnlLeftPadding.TabIndex = 7;
            // 
            // pnlRightPadding
            // 
            this.pnlRightPadding.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRightPadding.Location = new System.Drawing.Point(898, 0);
            this.pnlRightPadding.Name = "pnlRightPadding";
            this.pnlRightPadding.Size = new System.Drawing.Size(12, 441);
            this.pnlRightPadding.TabIndex = 8;
            // 
            // gbxButtons
            // 
            this.gbxButtons.Controls.Add(this.btnPrintandNew);
            this.gbxButtons.Controls.Add(this.btnPrintandClose);
            this.gbxButtons.Controls.Add(this.btnSaveAndNew);
            this.gbxButtons.Controls.Add(this.btnSaveAndClose);
            this.gbxButtons.Controls.Add(this.btnReset);
            this.gbxButtons.Controls.Add(this.btnCancel);
            this.gbxButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxButtons.Location = new System.Drawing.Point(12, 0);
            this.gbxButtons.Name = "gbxButtons";
            this.gbxButtons.Size = new System.Drawing.Size(886, 68);
            this.gbxButtons.TabIndex = 9;
            this.gbxButtons.TabStop = false;
            // 
            // btnPrintandNew
            // 
            this.btnPrintandNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnPrintandNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnPrintandNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnPrintandNew.FlatAppearance.BorderSize = 0;
            this.btnPrintandNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintandNew.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnPrintandNew.Location = new System.Drawing.Point(210, 22);
            this.btnPrintandNew.Name = "btnPrintandNew";
            this.btnPrintandNew.Size = new System.Drawing.Size(97, 33);
            this.btnPrintandNew.TabIndex = 5;
            this.btnPrintandNew.Text = "Print and New";
            this.btnPrintandNew.UseVisualStyleBackColor = false;
            this.btnPrintandNew.Visible = false;
            // 
            // btnPrintandClose
            // 
            this.btnPrintandClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnPrintandClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnPrintandClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnPrintandClose.FlatAppearance.BorderSize = 0;
            this.btnPrintandClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintandClose.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnPrintandClose.Location = new System.Drawing.Point(324, 22);
            this.btnPrintandClose.Name = "btnPrintandClose";
            this.btnPrintandClose.Size = new System.Drawing.Size(97, 33);
            this.btnPrintandClose.TabIndex = 4;
            this.btnPrintandClose.Text = "Print and Close";
            this.btnPrintandClose.UseVisualStyleBackColor = false;
            this.btnPrintandClose.Visible = false;
            // 
            // btnSaveAndNew
            // 
            this.btnSaveAndNew.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSaveAndNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndNew.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndNew.FlatAppearance.BorderSize = 0;
            this.btnSaveAndNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAndNew.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnSaveAndNew.Location = new System.Drawing.Point(438, 22);
            this.btnSaveAndNew.Name = "btnSaveAndNew";
            this.btnSaveAndNew.Size = new System.Drawing.Size(97, 33);
            this.btnSaveAndNew.TabIndex = 3;
            this.btnSaveAndNew.Text = "Save and New";
            this.btnSaveAndNew.UseVisualStyleBackColor = false;
            this.btnSaveAndNew.Visible = false;
            // 
            // btnSaveAndClose
            // 
            this.btnSaveAndClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSaveAndClose.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnSaveAndClose.FlatAppearance.BorderSize = 0;
            this.btnSaveAndClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaveAndClose.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnSaveAndClose.Location = new System.Drawing.Point(552, 22);
            this.btnSaveAndClose.Name = "btnSaveAndClose";
            this.btnSaveAndClose.Size = new System.Drawing.Size(97, 33);
            this.btnSaveAndClose.TabIndex = 2;
            this.btnSaveAndClose.Text = "Save and Close";
            this.btnSaveAndClose.UseVisualStyleBackColor = false;
            this.btnSaveAndClose.Visible = false;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnReset.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReset.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnReset.Location = new System.Drawing.Point(666, 22);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(97, 33);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnCancel.Location = new System.Drawing.Point(780, 22);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 33);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // pnlBottomPadding
            // 
            this.pnlBottomPadding.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomPadding.Location = new System.Drawing.Point(12, 429);
            this.pnlBottomPadding.Name = "pnlBottomPadding";
            this.pnlBottomPadding.Size = new System.Drawing.Size(886, 12);
            this.pnlBottomPadding.TabIndex = 10;
            // 
            // gbxDataGrid
            // 
            this.gbxDataGrid.Controls.Add(this.dgdPendingOrders);
            this.gbxDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxDataGrid.Location = new System.Drawing.Point(12, 68);
            this.gbxDataGrid.Name = "gbxDataGrid";
            this.gbxDataGrid.Size = new System.Drawing.Size(886, 361);
            this.gbxDataGrid.TabIndex = 16;
            this.gbxDataGrid.TabStop = false;
            this.gbxDataGrid.Text = "Pending Orders";
            // 
            // dgdPendingOrders
            // 
            this.dgdPendingOrders.AllowUserToAddRows = false;
            this.dgdPendingOrders.AllowUserToDeleteRows = false;
            this.dgdPendingOrders.AllowUserToOrderColumns = true;
            this.dgdPendingOrders.BackgroundColor = System.Drawing.Color.White;
            this.dgdPendingOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgdPendingOrders.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgdPendingOrders.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgdPendingOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgdPendingOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgdPendingOrders.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dgdPendingOrders.Location = new System.Drawing.Point(3, 19);
            this.dgdPendingOrders.Name = "dgdPendingOrders";
            this.dgdPendingOrders.ReadOnly = true;
            this.dgdPendingOrders.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgdPendingOrders.RowHeadersVisible = false;
            this.dgdPendingOrders.Size = new System.Drawing.Size(880, 339);
            this.dgdPendingOrders.TabIndex = 1;
            // 
            // Report
            // 
            this.AcceptButton = this.btnSaveAndNew;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(910, 441);
            this.Controls.Add(this.gbxDataGrid);
            this.Controls.Add(this.pnlBottomPadding);
            this.Controls.Add(this.gbxButtons);
            this.Controls.Add(this.pnlRightPadding);
            this.Controls.Add(this.pnlLeftPadding);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PatientInput";
            this.gbxButtons.ResumeLayout(false);
            this.gbxDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgdPendingOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeftPadding;
        private System.Windows.Forms.Panel pnlRightPadding;
        public System.Windows.Forms.GroupBox gbxButtons;
        public System.Windows.Forms.Button btnSaveAndNew;
        public System.Windows.Forms.Button btnSaveAndClose;
        public System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlBottomPadding;
        public System.Windows.Forms.Button btnPrintandNew;
        public System.Windows.Forms.Button btnPrintandClose;
        private System.Windows.Forms.GroupBox gbxDataGrid;
        public System.Windows.Forms.DataGridView dgdPendingOrders;
    }
}