﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Products
    {
        public List<Product> ProductsCollection;
        public Product SpurGears_D_P { get; set; }
        public Product SpurGears_P { get; set; }
        public Product SpurGears { get; set; }
        public Product CrusherSpares_D_P { get; set; }
        public Product CrusherSpares_P { get; set; }
        public Product CrusherSpares { get; set; }
        public Product BrakeShoes_D_P { get; set; }
        public Product BrakeShoes_P { get; set; }
        public Product BrakeShoes { get; set; }
        public Product RopeDrums_D_P { get; set; }
        public Product RopeDrums_P { get; set; }
        public Product RopeDrums { get; set; }
        public Product BrakeDrums_D_P { get; set; }
        public Product BrakeDrums_P { get; set; }
        public Product BrakeDrums { get; set; }
        public Product GearboxHousing { get; set; }
        public Product ThermostatHousing { get; set; }


        public Products()
        {
            Machines machines = new Machines();
            Workers workers = new Workers();

            SpurGears_D_P = new Product("SpurGears_D_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 20 * 60 * 1000 },
                    { machines.Pattern_Borer, 10 * 60 * 1000 },
                    { machines.Fettling_Grinder, 4 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Drawing_Artisan, 2 * 60 * 60 * 1000 },
                    { workers.Pattern_Artisan, 3 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 15 *60 * 1000 },
                    { workers.Fettling_Artisan, 6 * 60 * 1000 }
                });
            SpurGears_P = new Product("SpurGears_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 20 * 60 * 1000 },
                    { machines.Pattern_Borer, 10 * 60 * 1000 },
                    { machines.Fettling_Grinder, 4 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Pattern_Artisan, 3 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 15 *60 * 1000 },
                    { workers.Fettling_Artisan, 6 * 60 * 1000 }
                });
            SpurGears = new Product("SpurGears",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_Borer, 10 * 60 * 1000 },
                    { machines.Fettling_Grinder, 4 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Foundry_General, 15 *60 * 1000 },
                    { workers.Fettling_Artisan, 6 * 60 * 1000 }
                });

            CrusherSpares_D_P = new Product("CrusherSpares_D_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 10 * 60 * 1000 },
                    { machines.Fettling_Grinder, 5 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Drawing_Artisan, 2 * 60 * 60 * 1000 },
                    { workers.Pattern_Apprentice, 4 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 30 * 60 * 1000 },
                    { workers.Fettling_Artisan, 7 * 60 * 1000 }
                });
            CrusherSpares_P = new Product("CrusherSpares_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 10 * 60 * 1000 },
                    { machines.Fettling_Grinder, 5 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Pattern_Apprentice, 4 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 30 * 60 * 1000 },
                    { workers.Fettling_Artisan, 7 * 60 * 1000 }
                });
            CrusherSpares = new Product("CrusherSpares",
                new Dictionary<Machine, int>()
                {
                    { machines.Fettling_Grinder, 5 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Foundry_General, 30 * 60 * 1000 },
                    { workers.Fettling_Artisan, 7 * 60 * 1000 }
                });

            BrakeShoes_D_P = new Product("BrakeShoes_D_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_CircularSaw, 10 * 60 * 1000 },
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 5 * 60 * 1000 },
                    { machines.Fettling_Grinder, 5 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 35 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 40 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Drawing_Artisan, 1 * 60 * 60 * 1000 },
                    { workers.Pattern_General, 4 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 45 * 60 * 1000 },
                    { workers.Fettling_Artisan, 10 * 60 * 1000 },
                    { workers.MachineShop_General, 55 * 60 * 100 },
                    { workers.MachineShop_Artisan, 40 * 60 * 100 }
                });
            BrakeShoes_P = new Product("BrakeShoes_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_CircularSaw, 10 * 60 * 1000 },
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 5 * 60 * 1000 },
                    { machines.Fettling_Grinder, 5 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 35 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 40 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Pattern_General, 4 * 60 * 60 * 1000 },
                    { workers.Foundry_General, 45 * 60 * 1000 },
                    { workers.Fettling_Artisan, 10 * 60 * 1000 },
                    { workers.MachineShop_General, 55 * 60 * 100 },
                    { workers.MachineShop_Artisan, 40 * 60 * 100 }
                });
            BrakeShoes = new Product("BrakeShoes",
                new Dictionary<Machine, int>()
                {
                    { machines.Fettling_Grinder, 5 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 35 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 40 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Foundry_General, 45 * 60 * 1000 },
                    { workers.Fettling_Artisan, 10 * 60 * 1000 },
                    { workers.MachineShop_General, 55 * 60 * 100 },
                    { workers.MachineShop_Artisan, 40 * 60 * 100 }
                });

            RopeDrums_D_P = new Product("RopeDrums_D_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 10 * 60 * 1000 },
                    { machines.Pattern_Borer, 6 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 5 * 60 * 1000 },
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_HorizontalBorer, 6 * 60 * 60 * 1000 },
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Drawing_Artisan, 2 * 60 * 60 * 1000 },
                    { workers.Pattern_Artisan, 5 * 60 * 60 * 1000 },
                    { workers.Foundry_Artisan, 45 * 60 * 1000 },
                    { workers.Fettling_Apprentice, 20 * 60 * 1000 },
                    { workers.MachineShop_Artisan, 7 * 60 * 60 * 100 },
                });
            RopeDrums_P = new Product("RopeDrums_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 10 * 60 * 1000 },
                    { machines.Pattern_Borer, 6 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 5 * 60 * 1000 },
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_HorizontalBorer, 6 * 60 * 60 * 1000 },
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Pattern_Artisan, 5 * 60 * 60 * 1000 },
                    { workers.Foundry_Artisan, 45 * 60 * 1000 },
                    { workers.Fettling_Apprentice, 20 * 60 * 1000 },
                    { workers.MachineShop_Artisan, 7 * 60 * 60 * 100 },
                });
            RopeDrums = new Product("RopeDrums",
                new Dictionary<Machine, int>()
                {
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_HorizontalBorer, 6 * 60 * 60 * 1000 },
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Foundry_Artisan, 45 * 60 * 1000 },
                    { workers.Fettling_Apprentice, 20 * 60 * 1000 },
                    { workers.MachineShop_Artisan, 7 * 60 * 60 * 100 },
                });

            BrakeDrums_D_P = new Product("BrakeDrums_D_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_BobbinMachine, 5 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 8 * 60 * 1000 },
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 2 * 60 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 3 * 60 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Drawing_Artisan, 5 * 60 * 60 * 1000 },
                    { workers.Pattern_Artisan, 30 * 60 * 1000 },
                    { workers.Foundry_Artisan, 15 * 60 * 1000 },
                    { workers.Fettling_General, 2 * 60 * 60 * 1000 },
                    { workers.Fettling_Artisan, 3 * 60 * 60 * 100 },
                    { workers.Fettling_Apprentice, 20 * 60 * 100 }
                });
            BrakeDrums_P = new Product("BrakeDrums_P",
                new Dictionary<Machine, int>()
                {
                    { machines.Pattern_BandSaw, 8 * 60 * 1000 },
                    { machines.Pattern_BobbinMachine, 5 * 60 * 1000 },
                    { machines.Pattern_DiscMachine, 8 * 60 * 1000 },
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 2 * 60 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 3 * 60 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Pattern_Artisan, 30 * 60 * 1000 },
                    { workers.Foundry_Artisan, 15 * 60 * 1000 },
                    { workers.Fettling_General, 2 * 60 * 60 * 1000 },
                    { workers.Fettling_Artisan, 3 * 60 * 60 * 100 },
                    { workers.Fettling_Apprentice, 20 * 60 * 100 }
                });
            BrakeDrums = new Product("BrakeDrums",
                new Dictionary<Machine, int>()
                {
                    { machines.Fettling_Grinder, 20 * 60 * 1000 },
                    { machines.MachineShop_VerticalBorer, 2 * 60 * 60 * 1000 },
                    { machines.MachineShop_LatheMachine, 3 * 60 * 60 * 1000 },
                    { machines.MachineShop_DrillingMachine, 20 * 60 * 1000 }
                },
                new Dictionary<Worker, int>()
                {
                    { workers.Foundry_Artisan, 15 * 60 * 1000 },
                    { workers.Fettling_General, 2 * 60 * 60 * 1000 },
                    { workers.Fettling_Artisan, 3 * 60 * 60 * 100 },
                    { workers.Fettling_Apprentice, 20 * 60 * 100 }
                });

            ProductsCollection = new List<Product>();
            ProductsCollection.AddRange(new[] 
            {
                SpurGears_D_P, SpurGears_P, SpurGears,
                CrusherSpares_D_P, CrusherSpares_P, CrusherSpares,
                BrakeShoes_D_P, BrakeShoes_P, BrakeShoes,
                RopeDrums_D_P, RopeDrums_P, RopeDrums,
                BrakeDrums_D_P, BrakeDrums_P, BrakeDrums
                //GearboxHousing,
                //ThermostatHousing
            });
        }
    }
}
