﻿namespace ACE
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.rbnAppRibbon = new System.Windows.Forms.Ribbon();
            this.rtbHome = new System.Windows.Forms.RibbonTab();
            this.rplOrders = new System.Windows.Forms.RibbonPanel();
            this.btnNewOrder = new System.Windows.Forms.RibbonButton();
            this.btnPendingOrders = new System.Windows.Forms.RibbonButton();
            this.btnDeleteOrder = new System.Windows.Forms.RibbonButton();
            this.rplJobs = new System.Windows.Forms.RibbonPanel();
            this.btnNewJob = new System.Windows.Forms.RibbonButton();
            this.btnPendingJobs = new System.Windows.Forms.RibbonButton();
            this.btnCompletedJobs = new System.Windows.Forms.RibbonButton();
            this.rplSchedule = new System.Windows.Forms.RibbonPanel();
            this.btnViewSchedule = new System.Windows.Forms.RibbonButton();
            this.btnPrintSchedule = new System.Windows.Forms.RibbonButton();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.btnHomeShowHome = new System.Windows.Forms.RibbonButton();
            this.btnHomeChangeTheme = new System.Windows.Forms.RibbonButton();
            this.btnOffice2007Theme = new System.Windows.Forms.RibbonButton();
            this.btnOffice2010Theme = new System.Windows.Forms.RibbonButton();
            this.btnOffice2013Theme = new System.Windows.Forms.RibbonButton();
            this.btnHomeChangePassword = new System.Windows.Forms.RibbonButton();
            this.rtbReports = new System.Windows.Forms.RibbonTab();
            this.rplViewReports = new System.Windows.Forms.RibbonPanel();
            this.btnViewSummay = new System.Windows.Forms.RibbonButton();
            this.btnViewStations = new System.Windows.Forms.RibbonButton();
            this.btnViewResources = new System.Windows.Forms.RibbonButton();
            this.btnViewMachines = new System.Windows.Forms.RibbonButton();
            this.btnViewWorkers = new System.Windows.Forms.RibbonButton();
            this.btnViewOrders = new System.Windows.Forms.RibbonButton();
            this.btnViewJobs = new System.Windows.Forms.RibbonButton();
            this.ribbonComboBox2 = new System.Windows.Forms.RibbonComboBox();
            this.stpMainForm = new System.Windows.Forms.StatusStrip();
            this.pnlStatistics = new System.Windows.Forms.Panel();
            this.gbxReorderList = new System.Windows.Forms.GroupBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.gbxStatistics = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtBusyResources = new System.Windows.Forms.TextBox();
            this.txtAvailableResources = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblCompletedJobs = new System.Windows.Forms.Label();
            this.lblJobsBeingProcessed = new System.Windows.Forms.Label();
            this.lblScheduledJobs = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblLoggedOnUser = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblLoggedOnAs = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tmrCurrentTime = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.btnChangePassword = new System.Windows.Forms.RibbonButton();
            this.btnThemeOffice2013 = new System.Windows.Forms.RibbonButton();
            this.btnThemeOffice2010 = new System.Windows.Forms.RibbonButton();
            this.btnThemeOffice2007 = new System.Windows.Forms.RibbonButton();
            this.btnChangeTheme = new System.Windows.Forms.RibbonButton();
            this.pnlStatistics.SuspendLayout();
            this.gbxReorderList.SuspendLayout();
            this.gbxStatistics.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbnAppRibbon
            // 
            this.rbnAppRibbon.Cursor = System.Windows.Forms.Cursors.Default;
            this.rbnAppRibbon.Font = new System.Drawing.Font("Trebuchet MS", 9F);
            this.rbnAppRibbon.Location = new System.Drawing.Point(0, 0);
            this.rbnAppRibbon.Minimized = false;
            this.rbnAppRibbon.Name = "rbnAppRibbon";
            // 
            // 
            // 
            this.rbnAppRibbon.OrbDropDown.BorderRoundness = 8;
            this.rbnAppRibbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.rbnAppRibbon.OrbDropDown.Name = "File";
            this.rbnAppRibbon.OrbDropDown.Size = new System.Drawing.Size(0, 72);
            this.rbnAppRibbon.OrbDropDown.TabIndex = 0;
            this.rbnAppRibbon.OrbImage = null;
            this.rbnAppRibbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
            this.rbnAppRibbon.OrbText = "File";
            this.rbnAppRibbon.OrbVisible = false;
            this.rbnAppRibbon.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.rbnAppRibbon.Size = new System.Drawing.Size(1016, 157);
            this.rbnAppRibbon.TabIndex = 0;
            this.rbnAppRibbon.Tabs.Add(this.rtbHome);
            this.rbnAppRibbon.Tabs.Add(this.rtbReports);
            this.rbnAppRibbon.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.rbnAppRibbon.ThemeColor = System.Windows.Forms.RibbonTheme.Green;
            // 
            // rtbHome
            // 
            this.rtbHome.Panels.Add(this.rplOrders);
            this.rtbHome.Panels.Add(this.rplJobs);
            this.rtbHome.Panels.Add(this.rplSchedule);
            this.rtbHome.Panels.Add(this.ribbonPanel9);
            this.rtbHome.Text = "Home";
            // 
            // rplOrders
            // 
            this.rplOrders.ButtonMoreEnabled = false;
            this.rplOrders.ButtonMoreVisible = false;
            this.rplOrders.Items.Add(this.btnNewOrder);
            this.rplOrders.Items.Add(this.btnPendingOrders);
            this.rplOrders.Items.Add(this.btnDeleteOrder);
            this.rplOrders.Text = "Orders";
            // 
            // btnNewOrder
            // 
            this.btnNewOrder.Image = ((System.Drawing.Image)(resources.GetObject("btnNewOrder.Image")));
            this.btnNewOrder.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnNewOrder.SmallImage")));
            this.btnNewOrder.Text = "New Order";
            this.btnNewOrder.Click += new System.EventHandler(this.btnNewOrder_Click);
            // 
            // btnPendingOrders
            // 
            this.btnPendingOrders.Image = ((System.Drawing.Image)(resources.GetObject("btnPendingOrders.Image")));
            this.btnPendingOrders.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPendingOrders.SmallImage")));
            this.btnPendingOrders.Text = "Pending Orders";
            this.btnPendingOrders.Click += new System.EventHandler(this.btnPendingOrders_Click);
            // 
            // btnDeleteOrder
            // 
            this.btnDeleteOrder.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteOrder.Image")));
            this.btnDeleteOrder.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnDeleteOrder.SmallImage")));
            this.btnDeleteOrder.Text = "Delete Order";
            this.btnDeleteOrder.Click += new System.EventHandler(this.btnDeleteOrder_Click);
            // 
            // rplJobs
            // 
            this.rplJobs.ButtonMoreEnabled = false;
            this.rplJobs.ButtonMoreVisible = false;
            this.rplJobs.Items.Add(this.btnNewJob);
            this.rplJobs.Items.Add(this.btnPendingJobs);
            this.rplJobs.Items.Add(this.btnCompletedJobs);
            this.rplJobs.Text = "Jobs";
            // 
            // btnNewJob
            // 
            this.btnNewJob.Image = ((System.Drawing.Image)(resources.GetObject("btnNewJob.Image")));
            this.btnNewJob.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnNewJob.SmallImage")));
            this.btnNewJob.Text = "New Job";
            this.btnNewJob.Click += new System.EventHandler(this.btnNewJob_Click);
            // 
            // btnPendingJobs
            // 
            this.btnPendingJobs.Image = ((System.Drawing.Image)(resources.GetObject("btnPendingJobs.Image")));
            this.btnPendingJobs.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPendingJobs.SmallImage")));
            this.btnPendingJobs.Text = "Pending Jobs";
            this.btnPendingJobs.Click += new System.EventHandler(this.btnPendingJobs_Click);
            // 
            // btnCompletedJobs
            // 
            this.btnCompletedJobs.Image = ((System.Drawing.Image)(resources.GetObject("btnCompletedJobs.Image")));
            this.btnCompletedJobs.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnCompletedJobs.SmallImage")));
            this.btnCompletedJobs.Text = "Completed Jobs";
            this.btnCompletedJobs.Click += new System.EventHandler(this.btnCompletedJobs_Click);
            // 
            // rplSchedule
            // 
            this.rplSchedule.ButtonMoreEnabled = false;
            this.rplSchedule.ButtonMoreVisible = false;
            this.rplSchedule.Items.Add(this.btnViewSchedule);
            this.rplSchedule.Items.Add(this.btnPrintSchedule);
            this.rplSchedule.Text = "Schedule";
            // 
            // btnViewSchedule
            // 
            this.btnViewSchedule.Image = ((System.Drawing.Image)(resources.GetObject("btnViewSchedule.Image")));
            this.btnViewSchedule.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewSchedule.SmallImage")));
            this.btnViewSchedule.Text = "View Schedule";
            // 
            // btnPrintSchedule
            // 
            this.btnPrintSchedule.Image = ((System.Drawing.Image)(resources.GetObject("btnPrintSchedule.Image")));
            this.btnPrintSchedule.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnPrintSchedule.SmallImage")));
            this.btnPrintSchedule.Text = "Print Schedule";
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ButtonMoreEnabled = false;
            this.ribbonPanel9.ButtonMoreVisible = false;
            this.ribbonPanel9.Items.Add(this.btnHomeShowHome);
            this.ribbonPanel9.Items.Add(this.btnHomeChangeTheme);
            this.ribbonPanel9.Items.Add(this.btnHomeChangePassword);
            this.ribbonPanel9.Text = "Tools";
            this.ribbonPanel9.Visible = false;
            // 
            // btnHomeShowHome
            // 
            this.btnHomeShowHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeShowHome.Image")));
            this.btnHomeShowHome.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeShowHome.SmallImage")));
            this.btnHomeShowHome.Text = "Home";
            // 
            // btnHomeChangeTheme
            // 
            this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2007Theme);
            this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2010Theme);
            this.btnHomeChangeTheme.DropDownItems.Add(this.btnOffice2013Theme);
            this.btnHomeChangeTheme.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeChangeTheme.Image")));
            this.btnHomeChangeTheme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeChangeTheme.SmallImage")));
            this.btnHomeChangeTheme.Style = System.Windows.Forms.RibbonButtonStyle.DropDown;
            this.btnHomeChangeTheme.Text = "Theme";
            // 
            // btnOffice2007Theme
            // 
            this.btnOffice2007Theme.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnOffice2007Theme.Image = ((System.Drawing.Image)(resources.GetObject("btnOffice2007Theme.Image")));
            this.btnOffice2007Theme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOffice2007Theme.SmallImage")));
            this.btnOffice2007Theme.Text = "Office 2007";
            // 
            // btnOffice2010Theme
            // 
            this.btnOffice2010Theme.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnOffice2010Theme.Image = ((System.Drawing.Image)(resources.GetObject("btnOffice2010Theme.Image")));
            this.btnOffice2010Theme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOffice2010Theme.SmallImage")));
            this.btnOffice2010Theme.Text = "Office 2010";
            // 
            // btnOffice2013Theme
            // 
            this.btnOffice2013Theme.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnOffice2013Theme.Image = ((System.Drawing.Image)(resources.GetObject("btnOffice2013Theme.Image")));
            this.btnOffice2013Theme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOffice2013Theme.SmallImage")));
            this.btnOffice2013Theme.Text = "Office 2013";
            // 
            // btnHomeChangePassword
            // 
            this.btnHomeChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("btnHomeChangePassword.Image")));
            this.btnHomeChangePassword.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHomeChangePassword.SmallImage")));
            this.btnHomeChangePassword.Text = "Password";
            // 
            // rtbReports
            // 
            this.rtbReports.Panels.Add(this.rplViewReports);
            this.rtbReports.Text = "Reports";
            // 
            // rplViewReports
            // 
            this.rplViewReports.ButtonMoreEnabled = false;
            this.rplViewReports.ButtonMoreVisible = false;
            this.rplViewReports.Items.Add(this.btnViewSummay);
            this.rplViewReports.Items.Add(this.btnViewStations);
            this.rplViewReports.Items.Add(this.btnViewResources);
            this.rplViewReports.Items.Add(this.btnViewMachines);
            this.rplViewReports.Items.Add(this.btnViewWorkers);
            this.rplViewReports.Items.Add(this.btnViewOrders);
            this.rplViewReports.Items.Add(this.btnViewJobs);
            this.rplViewReports.Text = "View Reports";
            // 
            // btnViewSummay
            // 
            this.btnViewSummay.Image = ((System.Drawing.Image)(resources.GetObject("btnViewSummay.Image")));
            this.btnViewSummay.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewSummay.SmallImage")));
            this.btnViewSummay.Text = "Summary";
            // 
            // btnViewStations
            // 
            this.btnViewStations.Image = ((System.Drawing.Image)(resources.GetObject("btnViewStations.Image")));
            this.btnViewStations.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewStations.SmallImage")));
            this.btnViewStations.Text = "Stations";
            // 
            // btnViewResources
            // 
            this.btnViewResources.Image = ((System.Drawing.Image)(resources.GetObject("btnViewResources.Image")));
            this.btnViewResources.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewResources.SmallImage")));
            this.btnViewResources.Text = "Resources";
            // 
            // btnViewMachines
            // 
            this.btnViewMachines.Image = ((System.Drawing.Image)(resources.GetObject("btnViewMachines.Image")));
            this.btnViewMachines.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewMachines.SmallImage")));
            this.btnViewMachines.Text = "Machines";
            // 
            // btnViewWorkers
            // 
            this.btnViewWorkers.Image = ((System.Drawing.Image)(resources.GetObject("btnViewWorkers.Image")));
            this.btnViewWorkers.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewWorkers.SmallImage")));
            this.btnViewWorkers.Text = "Workers";
            // 
            // btnViewOrders
            // 
            this.btnViewOrders.Image = ((System.Drawing.Image)(resources.GetObject("btnViewOrders.Image")));
            this.btnViewOrders.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewOrders.SmallImage")));
            this.btnViewOrders.Text = "Orders";
            // 
            // btnViewJobs
            // 
            this.btnViewJobs.Image = ((System.Drawing.Image)(resources.GetObject("btnViewJobs.Image")));
            this.btnViewJobs.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnViewJobs.SmallImage")));
            this.btnViewJobs.Text = "Jobs";
            // 
            // ribbonComboBox2
            // 
            this.ribbonComboBox2.Text = "Colors ";
            this.ribbonComboBox2.TextBoxText = "";
            // 
            // stpMainForm
            // 
            this.stpMainForm.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.stpMainForm.Location = new System.Drawing.Point(0, 687);
            this.stpMainForm.Name = "stpMainForm";
            this.stpMainForm.Size = new System.Drawing.Size(1016, 22);
            this.stpMainForm.TabIndex = 3;
            this.stpMainForm.Text = "statusStrip1";
            // 
            // pnlStatistics
            // 
            this.pnlStatistics.Controls.Add(this.gbxReorderList);
            this.pnlStatistics.Controls.Add(this.gbxStatistics);
            this.pnlStatistics.Controls.Add(this.groupBox1);
            this.pnlStatistics.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlStatistics.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.pnlStatistics.Location = new System.Drawing.Point(0, 157);
            this.pnlStatistics.Name = "pnlStatistics";
            this.pnlStatistics.Size = new System.Drawing.Size(247, 530);
            this.pnlStatistics.TabIndex = 4;
            // 
            // gbxReorderList
            // 
            this.gbxReorderList.Controls.Add(this.lblInfo);
            this.gbxReorderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxReorderList.Location = new System.Drawing.Point(0, 490);
            this.gbxReorderList.Name = "gbxReorderList";
            this.gbxReorderList.Size = new System.Drawing.Size(247, 40);
            this.gbxReorderList.TabIndex = 2;
            this.gbxReorderList.TabStop = false;
            this.gbxReorderList.Visible = false;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(6, 16);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(134, 15);
            this.lblInfo.TabIndex = 5;
            this.lblInfo.Text = "Re-order Level Reached:";
            // 
            // gbxStatistics
            // 
            this.gbxStatistics.Controls.Add(this.groupBox2);
            this.gbxStatistics.Controls.Add(this.lblCompletedJobs);
            this.gbxStatistics.Controls.Add(this.lblJobsBeingProcessed);
            this.gbxStatistics.Controls.Add(this.lblScheduledJobs);
            this.gbxStatistics.Controls.Add(this.label5);
            this.gbxStatistics.Controls.Add(this.label3);
            this.gbxStatistics.Controls.Add(this.label2);
            this.gbxStatistics.Controls.Add(this.label1);
            this.gbxStatistics.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxStatistics.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxStatistics.Location = new System.Drawing.Point(0, 59);
            this.gbxStatistics.Name = "gbxStatistics";
            this.gbxStatistics.Size = new System.Drawing.Size(247, 431);
            this.gbxStatistics.TabIndex = 1;
            this.gbxStatistics.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtBusyResources);
            this.groupBox2.Controls.Add(this.txtAvailableResources);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(18, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(217, 289);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resources";
            // 
            // txtBusyResources
            // 
            this.txtBusyResources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBusyResources.BackColor = System.Drawing.Color.Honeydew;
            this.txtBusyResources.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBusyResources.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtBusyResources.Location = new System.Drawing.Point(8, 176);
            this.txtBusyResources.Multiline = true;
            this.txtBusyResources.Name = "txtBusyResources";
            this.txtBusyResources.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBusyResources.Size = new System.Drawing.Size(203, 104);
            this.txtBusyResources.TabIndex = 42;
            // 
            // txtAvailableResources
            // 
            this.txtAvailableResources.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAvailableResources.BackColor = System.Drawing.Color.Honeydew;
            this.txtAvailableResources.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAvailableResources.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtAvailableResources.Location = new System.Drawing.Point(8, 42);
            this.txtAvailableResources.Multiline = true;
            this.txtAvailableResources.Name = "txtAvailableResources";
            this.txtAvailableResources.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAvailableResources.Size = new System.Drawing.Size(203, 104);
            this.txtAvailableResources.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(5, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 15);
            this.label10.TabIndex = 40;
            this.label10.Text = "Available:";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(5, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 15);
            this.label11.TabIndex = 39;
            this.label11.Text = "Busy:";
            // 
            // lblCompletedJobs
            // 
            this.lblCompletedJobs.AutoSize = true;
            this.lblCompletedJobs.Location = new System.Drawing.Point(119, 97);
            this.lblCompletedJobs.Name = "lblCompletedJobs";
            this.lblCompletedJobs.Size = new System.Drawing.Size(15, 17);
            this.lblCompletedJobs.TabIndex = 8;
            this.lblCompletedJobs.Text = "0";
            // 
            // lblJobsBeingProcessed
            // 
            this.lblJobsBeingProcessed.AutoSize = true;
            this.lblJobsBeingProcessed.Location = new System.Drawing.Point(147, 71);
            this.lblJobsBeingProcessed.Name = "lblJobsBeingProcessed";
            this.lblJobsBeingProcessed.Size = new System.Drawing.Size(15, 17);
            this.lblJobsBeingProcessed.TabIndex = 7;
            this.lblJobsBeingProcessed.Text = "0";
            // 
            // lblScheduledJobs
            // 
            this.lblScheduledJobs.AutoSize = true;
            this.lblScheduledJobs.Location = new System.Drawing.Point(116, 45);
            this.lblScheduledJobs.Name = "lblScheduledJobs";
            this.lblScheduledJobs.Size = new System.Drawing.Size(15, 17);
            this.lblScheduledJobs.TabIndex = 6;
            this.lblScheduledJobs.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Statistics";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Completed Jobs:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Jobs Being Processed:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scheduled Jobs:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTime);
            this.groupBox1.Controls.Add(this.lblLoggedOnUser);
            this.groupBox1.Controls.Add(this.lblDate);
            this.groupBox1.Controls.Add(this.lblLoggedOnAs);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 59);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(164, 41);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(71, 15);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "10:09:25 AM";
            // 
            // lblLoggedOnUser
            // 
            this.lblLoggedOnUser.AutoSize = true;
            this.lblLoggedOnUser.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoggedOnUser.Location = new System.Drawing.Point(93, 12);
            this.lblLoggedOnUser.Name = "lblLoggedOnUser";
            this.lblLoggedOnUser.Size = new System.Drawing.Size(43, 15);
            this.lblLoggedOnUser.TabIndex = 2;
            this.lblLoggedOnUser.Text = "Admin";
            this.lblLoggedOnUser.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(3, 41);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(142, 15);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "Tuesday, 11 October 2016";
            // 
            // lblLoggedOnAs
            // 
            this.lblLoggedOnAs.AutoSize = true;
            this.lblLoggedOnAs.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoggedOnAs.Location = new System.Drawing.Point(3, 12);
            this.lblLoggedOnAs.Name = "lblLoggedOnAs";
            this.lblLoggedOnAs.Size = new System.Drawing.Size(84, 15);
            this.lblLoggedOnAs.TabIndex = 0;
            this.lblLoggedOnAs.Text = "Logged on as: ";
            this.lblLoggedOnAs.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(247, 157);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 530);
            this.panel3.TabIndex = 8;
            // 
            // tmrCurrentTime
            // 
            this.tmrCurrentTime.Enabled = true;
            this.tmrCurrentTime.Interval = 1000;
            this.tmrCurrentTime.Tick += new System.EventHandler(this.tmrCurrentTime_Tick);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.SmallImage = ((System.Drawing.Image)(resources.GetObject("button1.SmallImage")));
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "Update";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("btnChangePassword.Image")));
            this.btnChangePassword.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnChangePassword.SmallImage")));
            this.btnChangePassword.Text = "Change Password";
            // 
            // btnThemeOffice2013
            // 
            this.btnThemeOffice2013.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnThemeOffice2013.Image = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2013.Image")));
            this.btnThemeOffice2013.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2013.SmallImage")));
            this.btnThemeOffice2013.Text = "Office 2013";
            // 
            // btnThemeOffice2010
            // 
            this.btnThemeOffice2010.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnThemeOffice2010.Image = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2010.Image")));
            this.btnThemeOffice2010.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2010.SmallImage")));
            this.btnThemeOffice2010.Text = "Office 2010";
            // 
            // btnThemeOffice2007
            // 
            this.btnThemeOffice2007.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnThemeOffice2007.Image = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2007.Image")));
            this.btnThemeOffice2007.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnThemeOffice2007.SmallImage")));
            this.btnThemeOffice2007.Text = "Office 2007";
            // 
            // btnChangeTheme
            // 
            this.btnChangeTheme.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2007);
            this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2010);
            this.btnChangeTheme.DropDownItems.Add(this.btnThemeOffice2013);
            this.btnChangeTheme.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeTheme.Image")));
            this.btnChangeTheme.MaximumSize = new System.Drawing.Size(2, 2);
            this.btnChangeTheme.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnChangeTheme.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.btnChangeTheme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnChangeTheme.SmallImage")));
            this.btnChangeTheme.Style = System.Windows.Forms.RibbonButtonStyle.DropDown;
            this.btnChangeTheme.Text = "ribbonButton1";
            this.btnChangeTheme.ToolTip = "Change Theme";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1016, 709);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlStatistics);
            this.Controls.Add(this.stpMainForm);
            this.Controls.Add(this.rbnAppRibbon);
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Production Scheduler";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlStatistics.ResumeLayout(false);
            this.gbxReorderList.ResumeLayout(false);
            this.gbxReorderList.PerformLayout();
            this.gbxStatistics.ResumeLayout(false);
            this.gbxStatistics.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Ribbon rbnAppRibbon;
        private System.Windows.Forms.RibbonTab rtbHome;
        private System.Windows.Forms.RibbonPanel rplOrders;
        private System.Windows.Forms.RibbonButton button1;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonComboBox ribbonComboBox2;
        private System.Windows.Forms.RibbonButton btnPendingOrders;
        private System.Windows.Forms.RibbonButton btnNewOrder;
        private System.Windows.Forms.StatusStrip stpMainForm;
        private System.Windows.Forms.Panel pnlStatistics;
        private System.Windows.Forms.GroupBox gbxStatistics;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RibbonPanel rplJobs;
        private System.Windows.Forms.RibbonButton btnNewJob;
        private System.Windows.Forms.RibbonButton btnCompletedJobs;
        private System.Windows.Forms.RibbonButton btnPendingJobs;
        private System.Windows.Forms.RibbonTab rtbReports;
        private System.Windows.Forms.RibbonPanel rplViewReports;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.RibbonButton btnHomeShowHome;
        private System.Windows.Forms.RibbonButton btnHomeChangeTheme;
        private System.Windows.Forms.RibbonButton btnHomeChangePassword;
        private System.Windows.Forms.RibbonButton btnChangePassword;
        private System.Windows.Forms.RibbonButton btnThemeOffice2013;
        private System.Windows.Forms.RibbonButton btnThemeOffice2010;
        private System.Windows.Forms.RibbonButton btnThemeOffice2007;
        private System.Windows.Forms.RibbonButton btnChangeTheme;
        private System.Windows.Forms.RibbonButton btnOffice2007Theme;
        private System.Windows.Forms.RibbonButton btnOffice2010Theme;
        private System.Windows.Forms.RibbonButton btnOffice2013Theme;
        private System.Windows.Forms.Label lblLoggedOnAs;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Timer tmrCurrentTime;
        private System.Windows.Forms.Label lblLoggedOnUser;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblCompletedJobs;
        private System.Windows.Forms.Label lblJobsBeingProcessed;
        private System.Windows.Forms.Label lblScheduledJobs;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbxReorderList;
        private System.Windows.Forms.RibbonButton btnViewStations;
        private System.Windows.Forms.RibbonButton btnViewResources;
        private System.Windows.Forms.RibbonButton btnViewMachines;
        private System.Windows.Forms.RibbonButton btnViewWorkers;
        private System.Windows.Forms.RibbonButton btnViewOrders;
        private System.Windows.Forms.RibbonButton btnViewJobs;
        private System.Windows.Forms.RibbonButton btnViewSummay;
        private System.Windows.Forms.RibbonButton btnDeleteOrder;
        private System.Windows.Forms.RibbonPanel rplSchedule;
        private System.Windows.Forms.RibbonButton btnViewSchedule;
        private System.Windows.Forms.RibbonButton btnPrintSchedule;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtBusyResources;
        public System.Windows.Forms.TextBox txtAvailableResources;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }

}

