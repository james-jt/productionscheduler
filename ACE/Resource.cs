﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Resource
    { 
        public enum ResourceType { Machine, Worker }
        public string Name { get; set; }
        public string Description { get; set; }
        //public Station HostStation { get; set; }
        public int TotalInstances { get; set; }
        public int AvailableInstances { get; set; }
        public ResourceType Type { get; set; }
         
        public Queue<Job> Jobs { get; set; }

        public Resource(string name, string description, int totalInstances)
        {
            Name = name;
            Description = description;
            //HostStation = hostStation;
            TotalInstances = totalInstances;
            AvailableInstances = TotalInstances;
            Jobs = new Queue<Job>();
        }
    }
}
 