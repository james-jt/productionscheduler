﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Machines
    {
        public Machine Pattern_BandSaw { get; }
        public Machine Pattern_CircularSaw { get; }
        public Machine Pattern_DiscMachine { get; }
        public Machine Pattern_BobbinMachine { get; }
        public Machine Fettling_Grinder { get; }
        public Machine MachineShop_LatheMachine { get; }
        public Machine MachineShop_MillingMachine { get; }
        public Machine MachineShop_VerticalBorer { get; }
        public Machine MachineShop_HorizontalBorer { get; }
        public Machine MachineShop_DrillingMachine { get; }
        public Machine Pattern_Borer { get; }
        public List<Machine> MachinesCollection { get; set; }
         
        public Machines()
        {
            Pattern_BandSaw = new Machine("Band Saw", "Used in cutting out patterns.", 2);
            Pattern_CircularSaw = new Machine("Circular Saw", "Used in cutting out patterns.", 2);
            Pattern_DiscMachine = new Machine( "Disc Machine", "Used in cutting out discs.", 2);
            Pattern_BobbinMachine = new Machine( "Bobbin Machine", "Used in cutting out patterns.", 2);
            Fettling_Grinder = new Machine("Grinder", "Used in cutting out patterns.", 2);
            MachineShop_LatheMachine = new Machine("Lathe Machine", "Used in cutting out patterns.", 2);
            MachineShop_MillingMachine = new Machine("Milling Machine", "Used in cutting out patterns.", 2);
            MachineShop_VerticalBorer = new Machine("Vertical Borer", "Used in cutting out patterns.", 2);
            MachineShop_HorizontalBorer = new Machine("Horizontal Borer", "Used in cutting out patterns.", 2);
            MachineShop_DrillingMachine = new Machine("Drilling Machine", "Used in cutting out patterns.", 2);
            Pattern_Borer = new Machine("MachineShop_Borer", "Used for boring.", 2);
            MachinesCollection = new List<Machine>();
            MachinesCollection.AddRange(new[] { Pattern_BandSaw, Pattern_CircularSaw, Pattern_DiscMachine, Pattern_BobbinMachine, Fettling_Grinder, MachineShop_LatheMachine, MachineShop_MillingMachine, MachineShop_VerticalBorer, MachineShop_HorizontalBorer, MachineShop_DrillingMachine, Pattern_Borer });
        }

    }
}
