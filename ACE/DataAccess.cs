﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;

namespace ACE
{
    public class DataAccess
    {
        /*string connectionString = @"database=localhost:C:\JT-Projects\ACE\ACE\ProductionSchedulingDB.FDB;
        User=;Password=;DataSource=localhost;Port=3050;Dialect=3;Charset=NONE;
        Role=;Connection lifetime=15;Pooling=true;MinPoolSize=0;MaxPoolSize=50;Packet Size=8192;ServerType=0";*/
        string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Anole.mdf;Integrated Security=True;Connect Timeout=30";

        SqlConnection connection;
        private string sqlCommand;
        SqlCommand command;

        public DataAccess()
        {
            var testConnection = new SqlConnection(connectionString);
            try
            {
                testConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
            connection = testConnection;
            command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
        }


        #region Common Methods

        private DataSet ExecuteDatabaseQuery(string query)
        {
            if(connection == null) {
                return null;
            }
            try { command.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet resultTable = new DataSet();
                da.Fill(resultTable);
                return resultTable;
            }
            catch 
            {
                return null;
            }
        }

        private string ChangeToMySqlDate(DateTime date)
        {
            return date.Year + "/" + date.Month + "/" + date.Day;
        }

        #endregion


        #region Order Methods

        public bool CreateOrderRecord(object sender)
        {
            if (!(sender is Order)) return false;
            Order newOrder = sender as Order;
            string isScheduled = newOrder.IsScheduled == true ? "TRUE" : "FALSE";
            sqlCommand = string.Format("INSERT INTO ORDER_TBL VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}');",
                newOrder.OrderNumber,
                ChangeToMySqlDate(newOrder.OrderDate),
                newOrder.OrderedBy,
                newOrder.OrderProduct.Name,
                newOrder.OrderPriority,
                newOrder.Quantity,
                ChangeToMySqlDate(newOrder.DueDate),
                newOrder.OrderDimensions.Length,
                newOrder.OrderDimensions.Width,
                newOrder.OrderDimensions.Diameter,
                isScheduled);

            command.CommandText = sqlCommand;
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public DataSet ReadAllOrderRecords()
        {
            sqlCommand = "SELECT * FROM ORDER_TBL";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadAllPendingOrdersRecords()
        { 
            sqlCommand = "SELECT * FROM ORDER_TBL WHERE ISSCHEDULED = 'FALSE'";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadAllCompletedOrdersRecords()
        {
            sqlCommand = "SELECT * FROM ORDER_TBL WHERE ISSCHEDULED = 'TRUE'";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadOrderRecord(object sender)
        {
            sqlCommand = string.Format("SELECT * FROM ORDER_TBL WHERE ORDERNUMBER = '{0}';", sender.ToString());
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public bool DeleteOrderRecord(object sender)
        {
            sqlCommand = string.Format("DELETE FROM ORDER_TBL WHERE ORDERNUMBER = '{0}';", sender.ToString());
            command.CommandText = sqlCommand;
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Job Methods

        public bool CreateJobRecord(object sender)
        {
            if (!(sender is Job)) return false;
            Job newJob = sender as Job;

            StringBuilder workers = new StringBuilder();
            newJob.Workers.ForEach(w => workers.Append(w.Name + ","));
            workers = new StringBuilder(workers.ToString().Trim(','));
             
            StringBuilder machines = new StringBuilder();
            newJob.Machines.ForEach(m => machines.Append(m.Name + ","));
            machines = new StringBuilder(machines.ToString().Trim(','));

            sqlCommand = string.Format("INSERT INTO JOB (JOBNUMBER, ORDERNUMBER, ISCOMPLETE, WORKERS, MACHINES) VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}');",
                newJob.JobNumber,
                newJob.OrderNumber,
                newJob.JobPriority.ToString(), //Check if this saves as int or string and ammend schedule() in checkNewOrdersTimerCallback in frmMain and frmNewJob. Apply same logic to Order Methods
                newJob.IsComplete,
                workers,
                machines);

            command.CommandText = sqlCommand;
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataSet ReadAllJobRecords()
        {
            sqlCommand = "SELECT * FROM JOB";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadAllPendingJobsRecords()
        {
            sqlCommand = "SELECT * FROM JOB WHERE ISCOMPLETE = 'FALSE'";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadAllCompletedJobsRecords()
        {
            sqlCommand = "SELECT * FROM JOB WHERE ISCOMPLETE = 'TRUE'";
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public DataSet ReadJobRecord(object sender)
        {
            sqlCommand = string.Format("SELECT * FROM JOB WHERE JOBNUMBER = '{0}';", sender.ToString());
            return ExecuteDatabaseQuery(sqlCommand);
        }

        public bool DeleteJobRecord(object sender)
        {
            sqlCommand = string.Format("DELETE FROM JOB WHERE JOBNUMBER = '{0}';", sender.ToString());
            command.CommandText = sqlCommand;
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}
