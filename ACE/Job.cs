﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{ 
    public class Job
    { 
        public int JobNumber { get; set; }
        public int OrderNumber { get; set; }
        public Order.Priority JobPriority { get; set; }
        public DateTime EnqueueTime { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public bool IsComplete { get; set; }
        public List<Worker> Workers { get; set; }
        public List<Machine> Machines { get; set; }

    }
}
