﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE
{
    public partial class frmNewJob : Form
    {
        DataSet dsGrid;
        DataSet dsPendingOrders;
        public frmNewJob()
        {
            InitializeComponent();
            dsGrid = new DataSet();
            FillDataGrid();

            dsPendingOrders = new DataSet();
            dsPendingOrders = new DataAccess().ReadAllPendingOrdersRecords();
            FillOrderNumberComboBox();
        }

        #region Common Methods

        private void ClearForm()
        {
            cmbOrderNumber.Text = cmbOrderNumber.Items[0].ToString();
            cmbProduct.Text = "";
            cmbPriority.Text = "";
            cmbLength.Text = "";
            cmbWidth.Text = "";
            cmbDiameter.Text = "";
            txtOrderedBy.Text = "";
            txtWorkers.Text = "";
            txtMachines.Text = "";
            dtpDueDate.Text = "";
            dgdPendingJobs.Rows.Clear();

            cmbProduct.Focus();
        }

        private void FillOrderNumberComboBox()
        {
            if(dsPendingOrders != null && dsPendingOrders.Tables.Count > 0)
            {
                cmbOrderNumber.DataSource = dsPendingOrders.Tables[0];
                cmbOrderNumber.DisplayMember = dsPendingOrders.Tables[0].Columns["ORDERNUMBER"].ToString();
            }
        }
        private void FillDataGrid()
        {
            dsGrid = frmMain.dataAccess.ReadAllPendingJobsRecords();
            dgdPendingJobs.DataSource = dsGrid;
            if (dsGrid == null) return;
            if (dsGrid.Tables.Count > 0)
            {
                dgdPendingJobs.DataMember = dsGrid.Tables[0].ToString();
            }
        }

        private void UpdateDataGrid()
        {
            dgdPendingJobs.Update();
        }

        private void Schedule()
        {
            Job newJob = new Job();
            newJob.JobNumber = Convert.ToInt32(txtJobNumber.Text);
            newJob.OrderNumber = Convert.ToInt32(cmbOrderNumber.Text);
            switch (cmbPriority.Text)
            {
                case "VeryHigh":
                    newJob.JobPriority = Order.Priority.VeryHigh;
                    break;
                case "High":
                    newJob.JobPriority = Order.Priority.High;
                    break;
                case "VeryLow":
                    newJob.JobPriority = Order.Priority.VeryLow;
                    break;
                case "Low":
                    newJob.JobPriority = Order.Priority.Low;
                    break;
                default:
                    newJob.JobPriority = Order.Priority.Normal;
                    break;
            }
            newJob.IsComplete = false;

            Product selectedProduct = new Products().ProductsCollection.Where(p => p.Name == cmbProduct.Text).Select(p => p).First();
            selectedProduct.MachineRequirement.Keys.ToList().ForEach(m => newJob.Machines.Add(new Machines().MachinesCollection.Where(mc => mc.Name == m.Name).Select(mc => mc as Machine).First()));
            selectedProduct.WorkerRequirement.Keys.ToList().ForEach(w => newJob.Workers.Add(new Workers().WorkersCollection.Where(wc => wc.Name == w.Name).Select(wc => wc as Worker).First()));

            if (new DataAccess().CreateJobRecord(newJob))
            {
                MessageBox.Show("Job successfully scheduled.", "Operation Sucessful");
            }
            else
            {
                MessageBox.Show("Job was not scheduled.", "Error");
            }
        }
        #endregion


        private void frmNewJob_Load(object sender, EventArgs e)
        {
            do
            {
                txtJobNumber.Text = new Random().Next().ToString();
                DataSet ds = new DataSet();
                ds = frmMain.dataAccess.ReadOrderRecord(txtJobNumber.Text);
                if (ds == null) break;
                if (ds.Tables.Count < 1) break;
            } while (true);

        }

        private void cmbOrderNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataRow dr;// = dsPendingOrders.Tables[0].NewRow();
            //dsPendingOrders.Tables[0].PrimaryKey =  new DataColumn[] { dsPendingOrders.Tables[0].Columns["ORDERNUMBER"] };
            //dr = dsPendingOrders.Tables[0].Rows.Find(cmbOrderNumber.Text);

            foreach(DataRow dr in dsPendingOrders.Tables[0].Rows)
            {
                if(dr["ORDERNUMBER"].ToString() == cmbOrderNumber.Text)
                {
                    cmbProduct.Text = dr["PRODUCT"].ToString();
                    cmbPriority.Text = dr["ORDERPRIORITY"].ToString();
                    cmbLength.Text = dr["LENGTH"].ToString();
                    cmbWidth.Text = dr["WIDTH"].ToString();
                    cmbDiameter.Text = dr["DIAMETER"].ToString();
                    cmbQuantity.Text = dr["QUANTITY"].ToString();
                    dtpDueDate.Value = DateTime.Parse(dr["DUEDATE"].ToString());
                    txtOrderedBy.Text = dr["ORDEREDBY"].ToString();

                    txtMachines.Clear();
                    txtWorkers.Clear();
                    Product selectedProduct = new Products().ProductsCollection.Where(p => p.Name == cmbProduct.Text).Select(p => p).First();
                    selectedProduct.MachineRequirement.Keys.ToList().ForEach(m => txtMachines.AppendText(m.Name + '\n'));
                    selectedProduct.WorkerRequirement.Keys.ToList().ForEach(m => txtWorkers.AppendText(m.Name + '\n'));
                    break;
                }
            }
        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {
            Schedule();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearForm();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
