﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Stations
    {
        public Station Drawing { get; }
        public Station Pattern { get; }
        public Station Foundry { get; }
        public Station Fettling { get; }
        public Station MachineShop { get; }

        public Stations()
        {
            Workers workers = new Workers();
            Machines machines = new Machines();
            Drawing = new Station("Technical Drawing", "Responsible for production of drawings detailing techical specs.", 
                workers.WorkersCollection.Where(w => w.Name.StartsWith("Drawing")).OrderBy(w => w).Select(w => w).ToList(),
                machines.MachinesCollection.Where(w => w.Name.StartsWith("Drawing")).OrderBy(w => w).Select(w => w).ToList());
            Pattern = new Station("Pattern Making", "Responsible for production of patterns from drawings.", 
                workers.WorkersCollection.Where(w => w.Name.StartsWith("Pattern")).OrderBy(w => w).Select(w => w).ToList(),
                machines.MachinesCollection.Where(w => w.Name.StartsWith("Pattern")).OrderBy(w => w).Select(w => w).ToList());
            Foundry = new Station("Foundry", "Responsible for smelting.", 
                workers.WorkersCollection.Where(w => w.Name.StartsWith("Foundry")).OrderBy(w => w).Select(w => w).ToList(),
                machines.MachinesCollection.Where(w => w.Name.StartsWith("Foundry")).OrderBy(w => w).Select(w => w).ToList());
            Fettling = new Station("Technical Drawing", "Responsible for smoothing out edges.", 
                workers.WorkersCollection.Where(w => w.Name.StartsWith("Fettling")).OrderBy(w => w).Select(w => w).ToList(),
                machines.MachinesCollection.Where(w => w.Name.StartsWith("Fettling")).OrderBy(w => w).Select(w => w).ToList());
            MachineShop = new Station("Technical Drawing", "Responsible for coming up with the product's final geometry.", 
                workers.WorkersCollection.Where(w => w.Name.StartsWith("MachineShop")).OrderBy(w => w).Select(w => w).ToList(),
                machines.MachinesCollection.Where(w => w.Name.StartsWith("MachineShop")).OrderBy(w => w).Select(w => w).ToList());
        }
    }
}
