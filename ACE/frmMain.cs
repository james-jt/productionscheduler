﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.RibbonHelpers;

namespace ACE
{
    public partial class frmMain : Form
    {
        public static DataAccess dataAccess = new DataAccess();

        frmHome homeForm;
        string Title = "Production Scheduler";
        Queue<Job> JobQueue;
        Workers workers;
        Machines machines;
        System.Threading.Timer checkNewOrdersTimerCallback;
        const int timeScale = 1000;
        int completedJobs = 0;
        int scheduledJobs = 0;
        int jobsBeingProcessed = 0;
        public frmMain()
        {
            InitializeComponent();
            //dataAccess = new DataAccess();

            homeForm = new frmHome();
            homeForm.MdiParent = this;
            homeForm.Dock = DockStyle.Fill;
            this.Text = Title + " - Home";
            homeForm.Show();
            LoadTheme();

            JobQueue = new Queue<Job>();
            workers = new Workers();
            machines = new Machines();

            lblDate.Text = System.DateTime.Now.ToLongDateString();
            lblTime.Text = System.DateTime.Now.ToLongTimeString();
            checkNewOrdersTimerCallback = new System.Threading.Timer(async (state) =>
            {
                DataSet ds = new DataSet();
                ds = dataAccess.ReadAllPendingJobsRecords();
                if (ds == null) return;
                if (ds.Tables.Count < 1) return;
                if(ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count < 8) return;
                    if (ds.Tables[0].Rows.Count < 10) lblInfo.Text = "Consider placing order for items to stock.";
                    else
                    {
                        //Sort records by order priority
                        DataView dv = ds.Tables[0].DefaultView;
                        dv.Sort = "JOBPRIORITY DESC";
                        DataTable sortedDT = dv.ToTable();

                        //Queue jobs for processing.
                        foreach (DataRow dr in sortedDT.Rows)
                        {
                            Job newJob = new Job();
                            newJob.JobNumber = Convert.ToInt32(dr["JOBNUMBER"].ToString());
                            newJob.OrderNumber = Convert.ToInt32(dr["ORDERNUMBER"].ToString());
                            newJob.EnqueueTime = DateTime.Now;
                            newJob.StartTime = new DateTime();
                            newJob.FinishTime = new DateTime();
                            newJob.IsComplete = (dr["ISCOMPLETE"].ToString() == "TRUE") ? true : false;
                            string[] tempMachine = dr["MACHINES"].ToString().Split(',');
                            tempMachine.ToList().ForEach(m => newJob.Machines.Add(new Machines().MachinesCollection.Where(mc => mc.Name == m).Select(mc => mc as Machine).First()));
                            string[] tempWorker = dr["WORKERS"].ToString().Split(',');
                            tempWorker.ToList().ForEach(w => newJob.Workers.Add(new Workers().WorkersCollection.Where(wc => wc.Name == w).Select(wc => wc as Worker).First()));
                            JobQueue.Enqueue(newJob);
                        }
                        await PrimarySchedulerAsync();
                    }
                }
            }, null, 0, 10000);
        }

        #region Common Methods

        private void ClearPnlMain()
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == typeof(frmHome))
                {
                    continue;
                }
                f.Close();
            }
        }

        private void SetUpChildForm(Form child, string caption)
        {

            child.MdiParent = this;
            child.Dock = DockStyle.Fill;
            this.Text = this.Title + " - " + caption;
            child.Show();
            Application.DoEvents();
        }

        private void LoadTheme()
        {
            if (this.rbnAppRibbon.OrbStyle == RibbonOrbStyle.Office_2013)
            {
                this.BackColor = Color.White;
            }
            else
            {
                this.BackColor = SystemColors.GradientInactiveCaption;
            }
            this.stpMainForm.BackColor = this.BackColor;
            this.pnlStatistics.BackColor = this.BackColor;
        }

        private Task PrimarySchedulerAsync()
        {
            return Task.Run(() =>
            {
                if (JobQueue.Count < 1) return;
                while (JobQueue.Count > 0)
                {
                    Job jobToSchedule = new Job();
                    jobToSchedule = JobQueue.Dequeue();

                    //Drawing
                    if (jobToSchedule.Workers.Any(w => w.Name.StartsWith("Drawing")))
                    {
                        workers.Drawing_Artisan.Jobs.Enqueue(jobToSchedule);
                        Interlocked.Increment(ref scheduledJobs);
                        return;
                    }

                    //Pattern
                    if (jobToSchedule.Workers.Any(w => w.Name.StartsWith("Pattern")))
                    {
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Artisan")))
                        {
                            workers.Pattern_Artisan.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Apprentice")))
                        {
                            workers.Pattern_Apprentice.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("General")))
                        {
                            workers.Pattern_General.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                    }

                    //Foundry
                    if (jobToSchedule.Workers.Any(w => w.Name.StartsWith("Foundry")))
                    {
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Artisan")))
                        {
                            workers.Foundry_Artisan.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Apprentice")))
                        {
                            workers.Foundry_Apprentice.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("General")))
                        {
                            workers.Foundry_General.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                    }

                    //Fettling
                    if (jobToSchedule.Workers.Any(w => w.Name.StartsWith("Fettling")))
                    {
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Artisan")))
                        {
                            workers.Fettling_Artisan.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Apprentice")))
                        {
                            workers.Fettling_Apprentice.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("General")))
                        {
                            workers.Fettling_General.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                    }

                    //MachineShop
                    if (jobToSchedule.Workers.Any(w => w.Name.StartsWith("MachineShop")))
                    {
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Artisan")))
                        {
                            workers.MachineShop_Artisan.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("Apprentice")))
                        {
                            workers.MachineShop_Apprentice.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                        if (jobToSchedule.Workers.Any(w => w.Name.EndsWith("General")))
                        {
                            workers.MachineShop_General.Jobs.Enqueue(jobToSchedule);
                            Interlocked.Increment(ref scheduledJobs);
                            return;
                        }
                    }
                }
            });
        }

        //This is a delegate to be associated with each resource and must be called in an infinite loop to process jobs
        //Find a clever way to start all resources simultaneously.
        private void ProcessJob(Job job, Worker resource)
        {
            job.StartTime = DateTime.Now;
            Interlocked.Decrement(ref scheduledJobs);
            Interlocked.Increment(ref jobsBeingProcessed);
            string productID = dataAccess.ReadAllPendingOrdersRecords().Tables[0].Rows.Find(job.OrderNumber)["PRODUCT"].ToString();
            Product product = new Products().ProductsCollection.Where(p => p.Name == productID).Select(p => p).First();
            System.Threading.Timer ProcessJob = new System.Threading.Timer((state) => 
            {
                job.FinishTime = DateTime.Now;
                Interlocked.Increment(ref completedJobs);
                //Update "JobSchedule" table via associated object
                SecondarySchedulerCallbackAsync(job); //Should we await this method?
                if(resource.Jobs.Count > 0)
                {
                    this.ProcessJob(resource.Jobs.Dequeue(), resource);
                }
            }, null, product.WorkerRequirement.Where(r => r.Key == resource).Select(r => r.Value).First() / timeScale, Timeout.Infinite);

        }
        
        //To be called by each resource when it finishes processing its current task
        private Task SecondarySchedulerCallbackAsync(Job partiallyProcessedJob)
        {
            return Task.Run(() =>
            { //If job is not complete schedule it for the next resource, otherwise flag it as complete and write it to db

            });
        }
        #endregion

        private void tmrCurrentTime_Tick(object sender, EventArgs e)
        {
            lblDate.Text = System.DateTime.Now.ToLongDateString();
            lblTime.Text = System.DateTime.Now.ToLongTimeString();

            lblScheduledJobs.Text = scheduledJobs.ToString();
            lblJobsBeingProcessed.Text = JobQueue.Count.ToString(); //remember to update this
            lblCompletedJobs.Text = completedJobs.ToString();
            //Update resources section
            txtAvailableResources.Visible = false;
            txtBusyResources.Visible = false;
            txtAvailableResources.Clear();
            txtBusyResources.Clear();
            workers.WorkersCollection.Where(w => w.Jobs.Count > 0).Select(w => w).ToList().ForEach(x => txtBusyResources.AppendText(x.Name + '\n'));
            workers.WorkersCollection.Where(w => w.Jobs.Count == 0).Select(w => w).ToList().ForEach(x => txtAvailableResources.AppendText(x.Name + '\n'));
            txtAvailableResources.Visible = true;
            txtBusyResources.Visible = true;
        }

        private void btnNewOrder_Click(object sender, EventArgs e)
        {
            ClearPnlMain();
            frmNewOrder newOrder = new frmNewOrder();
            SetUpChildForm(newOrder, btnNewOrder.Text);
        }

        private void btnPendingOrders_Click(object sender, EventArgs e)
        {
            ClearPnlMain();
            frmPendingOrders pendingOrders = new frmPendingOrders();
            SetUpChildForm(pendingOrders, btnViewOrders.Text);

        }

        private void btnDeleteOrder_Click(object sender, EventArgs e)
        {
            frmDelete deleteOrder = new frmDelete();
            deleteOrder.ShowDialog();
        }

        private void btnNewJob_Click(object sender, EventArgs e)
        {
            ClearPnlMain();
            frmNewJob newJob = new frmNewJob();
            SetUpChildForm(newJob, btnNewJob.Text);
        }

        private void btnPendingJobs_Click(object sender, EventArgs e)
        {
            ClearPnlMain();
            frmPendingJobs pendingJobs = new frmPendingJobs();
            SetUpChildForm(pendingJobs, btnPendingJobs.Text);

        }

        private void btnCompletedJobs_Click(object sender, EventArgs e)
        {
            ClearPnlMain();
            frmCompletedJobs completedJobs = new frmCompletedJobs();
            SetUpChildForm(completedJobs, btnPendingJobs.Text);
        }
    }
}

