﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Station
    { 
        public string FullName { get; set; }
        public string Description { get; set; }
        public List<Worker> Workers { get; set; }
        public List<Machine> Machines { get; set; }
         
        public List<Resource> Resources { get; set; }

        public Station(string name, string description, List<Worker> workers, List<Machine> machines)
        {
            FullName = name;
            Description = description;
            Workers = workers;
            Machines = machines;
            Resources.AddRange(Workers);
            Resources.AddRange(Machines);
        }
    }
}
