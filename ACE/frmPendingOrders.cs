﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ACE
{
    public partial class frmPendingOrders : Form
    {
        DataSet ds;
        DataTable dt;
        public frmPendingOrders()
        {
            InitializeComponent();
            ds = new DataSet();
            FillDataGrid();
        }

        private void FillDataGrid()
        {
            ds = frmMain.dataAccess.ReadAllPendingOrdersRecords();
            if (ds == null) return;
            if (ds.Tables.Count < 1) return;
            dgdPendingOrders.DataSource = ds;
            dgdPendingOrders.DataMember = ds.Tables[0].ToString();
        }

    }
}
