﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACE
{
    public class Product
    {
        public string Name { get; set; }
        public Dictionary<Machine, int> MachineRequirement { get; set; }
        public Dictionary<Worker, int> WorkerRequirement { get; set; }

        public Product(string name, Dictionary<Machine, int> machineRequirement, Dictionary<Worker, int> workerRequirement)
        {
            Name = name;
            MachineRequirement = machineRequirement;
            WorkerRequirement = workerRequirement;
        }
    }
}
 